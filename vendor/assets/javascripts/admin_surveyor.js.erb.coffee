#Admin functions to dynamically add fields from template based on type

$(document).ready ->
  $('body').on "change", ".question_kind_selector", (e) ->
    template = $(this).val()
    container = $(this).parent().parent()
    question_num = $(this).attr('id').split(/_/)[4]
    option_num = 0
    if template == 'free-text'
      container.find('.has_many_add').hide()
      container.find('.has_many_remove').hide()
    else
      container.find('.has_many_add').show()
      container.find('.has_many_remove').show()
    $.get "/surveyor/template?template="+template+"&one="+question_num+"&two="+option_num, (data) ->
      container.find(".has_many_container .has_many_fields").remove()
      container.find(".has_many_container h3").after data
      return

