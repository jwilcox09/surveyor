Surveyor::Engine.routes.draw do

  resources :attempts, :only => [:create, :show]
  get 'attempts/new/:survey_id', to: 'attempts#new', as: 'new_attempt'
  get 'template' => 'surveyors#get_template'

end
