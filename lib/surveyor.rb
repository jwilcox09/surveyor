require 'surveyor/engine'
require 'surveyor/version'
require 'surveyor/active_record'

ActiveRecord::Base.send(:include, Surveyor::ActiveRecord)
