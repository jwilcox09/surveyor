class CreateSurvey < ActiveRecord::Migration
  def self.up

    # survey surveys logic
    create_table :surveyor_surveys do |t|
      t.string  :name
      t.text    :description
      t.integer :attempts_number, :default => 0
      t.boolean :finished, :default => false
      t.boolean :active, :default => false
      t.boolean :mandatory, :default => false

      t.timestamps
    end

    create_table :surveyor_questions do |t|
      t.integer :survey_id
      t.string  :text
      t.string :kind

      t.timestamps
    end

    create_table :surveyor_options do |t|
      t.integer :question_id
      t.string :text
      t.boolean :open_text

      t.timestamps
    end

    # survey answer logic
    create_table :surveyor_attempts do |t|
      t.belongs_to :participant, :polymorphic => true
      t.integer    :survey_id
    end

    create_table :surveyor_answers do |t|
      t.integer    :attempt_id
      t.integer    :question_id
      t.integer    :option_id
      t.text         :open_text
      t.timestamps
    end
  end

  def self.down
    drop_table :surveyor_surveys
    drop_table :surveyor_questions
    drop_table :surveyor_options

    drop_table :surveyor_attempts
    drop_table :surveyor_answers
  end
end
