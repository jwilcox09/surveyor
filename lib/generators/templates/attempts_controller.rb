class Surveyor::AttemptsController < ApplicationController

  helper Rails.application.routes.url_helpers

  before_filter :normalize_attempts_data, :only => :create

  def new
    if params[:survey_id]
      @survey =  Surveyor::Survey.find(params[:survey_id])
    else
      @survey =  Surveyor::Survey.active.first
    end
    @participant = current_user

    unless @survey.nil?
      @attempt = @survey.attempts.new
      @attempt.answers.build
    end
  end

  def create
    if defined? params[:survey_attempt][:survey_id]
      @survey = Surveyor::Survey.find(params[:survey_attempt][:survey_id])
      @attempt = @survey.attempts.new(params[:survey_attempt])
      @attempt.participant = current_user

      if @attempt.valid? && @attempt.save
        redirect_to Surveyor::Engine.routes.url_helpers.attempt_path(@attempt.id), alert: I18n.t("attempts_controller.#{action_name}")
      else
        render :action => :new
      end
    else
      render :action => :new
    end
  end

  def show
    @attempt = Surveyor::Attempt.find(params[:id])
    @survey = Surveyor::Survey.find(@attempt.survey_id)
  end

  private

  def normalize_attempts_data
    normalize_data!(params[:survey_attempt][:answers_attributes])
  end

  def normalize_data!(hash)

    multiple_answers = []
    last_key = hash.keys.last.to_i

    hash.keys.each do |k|
      hash[k]['open_text'] = hash[k]['open_text'].first unless hash[k]['open_text'].blank?
      if hash[k]['option_id'].is_a?(Array)
        if hash[k]['option_id'].size == 1
          hash[k]['option_id'] = hash[k]['option_id'][0]
          next
        else
          multiple_answers <<  k if hash[k]['option_id'].size > 1
        end
      end
    end

    multiple_answers.each do |k|
      hash[k]['option_id'][1..-1].each do |o|
        last_key += 1
        hash[last_key.to_s] = hash[k].merge('option_id' => o)
      end
      hash[k]['option_id'] = hash[k]['option_id'].first
    end
  end
end
