ActiveAdmin.register Surveyor::Survey do
  menu :label => I18n.t("surveys")

  filter  :name,
    :as => :select,
  :collection => proc {
    Surveyor::Survey.select("distinct(name)").collect { |c|
      [c.name, c.name]
    }
  }
  filter :active,
    :as => :select,
    :collection => ["true", "false"]

  filter :created_at

  index do
    column :name
    column :description
    column :active
    column :attempts_number
    column :finished
    column :created_at
    actions
  end

  form do |f|
    f.inputs I18n.t("survey_details") do
      f.input  :name
      f.input  :description
      f.input  :active, :as => :select, :collection => ["true", "false"]
      f.input :mandatory, :as => :select, :collection => ["true", "false"]
      f.input  :attempts_number
    end
    f.inputs I18n.t("questions") do
      f.has_many :questions, allow_destroy: true do |q|
        q.input :text
        if q.object.new_record?
          q.input :kind, as: :select, collection: Surveyor::Question::QUESTION_KINDS, :input_html => { class: "question_kind_selector" }
        else
          q.input :kind, as: :select, collection: Surveyor::Question::QUESTION_KINDS, :input_html => { class: "question_kind_selector", disabled: true }
        end
        q.has_many :options, allow_destroy: true do |a|
          a.input  :text unless a.object.open_text == true
        end unless q.object.kind == 'free-text'
      end
    end
    f.actions
  end

  show do
      render "show"
    end

end
