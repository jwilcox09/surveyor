module Surveyor
  module Generators
    class InstallGenerator < Rails::Generators::Base
      source_root File.expand_path("../../templates", __FILE__)

      def copy_migration
        unless surveyor_migration_already_exists?
          timestamp_number = Time.now.utc.strftime("%Y%m%d%H%M%S").to_i
          copy_file "migration.rb", "db/migrate/#{timestamp_number}_create_survey.rb"
        end
      end

      def copy_app_files
        copy_file "active_admin.rb", "app/admin/survey.rb"
        prefix = "/surveyor"
        template "attempts_controller.rb", "app/controllers#{prefix}/attempts_controller.rb"
        template "helper.rb", "app/helpers#{prefix}/surveys_helper.rb"
        directory "attempts_views", "app/views#{prefix}/attempts", :recursive => true
        generate_routes
        generate_ability
        generate_user_relation
      end

      private

        def generate_routes
          content = <<-CONTENT

          mount Surveyor::Engine => "/surveyor"
          CONTENT

          inject_into_file "config/routes.rb", "\n#{content}",
            :after => "#{Rails.application.class.to_s}.routes.draw do"
        end

        def generate_ability
          content = <<-CONTENT
          can :new, Surveyor::Attempt
          can :create, Surveyor::Attempt
          can :show, Surveyor::Attempt, :participant_id => user.id
          CONTENT

          inject_into_file "app/models/ability.rb", "\n#{content}",
            :after => "if user"
        end

        def generate_user_relation
          content = <<-CONTENT
          has_many :surveyor_attempts, foreign_key: :participant_id, dependent: :destroy
          CONTENT

          inject_into_file "app/models/user.rb", "\n#{content}",
            :after => "has_many :user_points, foreign_key: :user_id, dependent: :destroy"
        end

        def surveyor_migration_already_exists?
           Dir.glob("#{File.join(destination_root, File.join("db", "migrate"))}/[0-9]*_*.rb").grep(/\d+_create_survey.rb$/).first
        end
    end
  end
end
