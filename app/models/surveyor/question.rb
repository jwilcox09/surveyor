class Surveyor::Question < ActiveRecord::Base

  QUESTION_KINDS = ["single-select", 'multi-select', 'free-text', 'multi-select-option', 'single-select-option', 'one-to-ten']

  self.table_name = "surveyor_questions"

  acceptable_attributes :text, :survey, :kind, :options_attributes => Surveyor::Option::AccessibleAttributes

  # relations
  belongs_to :survey
  has_many   :options, :dependent => :destroy
  accepts_nested_attributes_for :options,
    :reject_if => ->(a) { a[:text].blank? && a[:kind] != 'free-text' },
    :allow_destroy => true

  # validations
  validates :text, :presence => true, :allow_blank => false

  def correct_options
    return options.correct
  end

  def incorrect_options
    return options.incorrect
  end
end
