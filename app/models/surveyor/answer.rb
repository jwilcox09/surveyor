class Surveyor::Answer < ActiveRecord::Base

  self.table_name = "surveyor_answers"

  acceptable_attributes :attempt, :option, :option_id, :question, :question_id, :open_text

  # associations
  belongs_to :attempt
  belongs_to :option
  belongs_to :question

  # validations
  validates :option_id, :question_id, :presence => true
  validates :option_id, :uniqueness => { :scope => [:attempt_id, :question_id] }

end
