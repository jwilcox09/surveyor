class Surveyor::Survey < ActiveRecord::Base

  self.table_name = "surveyor_surveys"

  acceptable_attributes :name, :description,
    :finished,
    :active,
    :attempts_number,
    :mandatory,
    :questions_attributes => Surveyor::Question::AccessibleAttributes

  # relations
  has_many :attempts,  :dependent => :destroy
  has_many :questions, :dependent => :destroy
  accepts_nested_attributes_for :questions,
    :reject_if => ->(q) { q[:text].blank? },
    :allow_destroy => true

  # scopes
  scope :active,   -> { where(:active => true) }
  scope :inactive, -> { where(:active => false) }

  # validations
  validates :attempts_number, :numericality => { :only_integer => true, :greater_than => -1 }
  validates :description, :name, :presence => true, :allow_blank => false
  validate  :check_active_requirements

  # Class methods
  def self.duplicate(old_survey_id)
    new_survey  = find(old_survey_id).deep_clone :include => {:questions => :options}
    new_survey.save
    new_survey
  end


  def avaliable_for_participant?(participant)
    current_number_of_attempts = self.attempts.for_participant(participant).size
    upper_bound = self.attempts_number
    return !((current_number_of_attempts >= upper_bound) && (upper_bound != 0))
  end

  private

  # a surveys only can be activated if has one or more questions
  def check_active_requirements
    errors.add(:active, "Survey without questions cannot be activated") if self.active && self.questions.empty?
  end
end
