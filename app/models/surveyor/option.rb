class Surveyor::Option < ActiveRecord::Base

  self.table_name = "surveyor_options"

  acceptable_attributes :text, :open_text

  #relations
  belongs_to :question

  # validations
  validates :text, :presence => true


  def to_s
    return self.text
  end

end
