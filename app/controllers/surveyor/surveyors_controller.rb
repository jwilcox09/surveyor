module Surveyor
  class SurveyorsController < ApplicationController

    def get_template
      render :partial =>"surveyor/admin/surveyor_surveys/#{params[:template]}_template",  locals: {one: params[:one], two: params[:two]}
    end

  end
end
