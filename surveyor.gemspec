# -*- encoding: utf-8 -*-
$:.push File.expand_path('../lib', __FILE__)
require "surveyor/version"

Gem::Specification.new do |s|

  # Description Meta
  s.name = "surveyor"
  s.version = "0.1"
  s.platform    = Gem::Platform::RUBY
  s.authors = ["Joshua Wilcox"]
  s.email = ["me@joshuawilcox.net"]
  s.homepage = "https://bitbucket.org/jwilcox09/surveyor"
  s.summary = %q{Surveys/Questionnaires in rails}
  s.description = %q{A libraray for creating surveys and questionnaires in your rails application}
  s.licenses = "MIT"
  s.rubyforge_project = "surveyor"
  s.rubygems_version = '>= 1.9.2'

  # Load Paths
  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- test/*`.split("\n")
  s.require_paths = ["lib"]

  # Dependencies
  s.add_dependency("activerecord", [">= 3.2"])
  s.add_dependency("simple_form")
  s.add_dependency("deep_cloneable", ["~> 2.0.0"])

  s.add_development_dependency("rspec")
  s.add_development_dependency("factory_girl")
end
